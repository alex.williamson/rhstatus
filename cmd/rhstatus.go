package cmd

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/cobra"
	gitlab "github.com/xanzy/go-gitlab"
	config "gitlab.com/prarit/rhstatus/internal/config"
	lab "gitlab.com/prarit/rhstatus/internal/gitlab"
	"gitlab.com/prarit/rhstatus/internal/postfix"
)

var username string
var project string
var noRole = "        "

var (
	todoType         string
	todoNumRet       string
	targetType       string
	todoPretty       bool
	todoAll          bool
	addlabel         string
	removelabel      string
	fullmode         bool
	interest         bool
	mrPadding        int
	mrIIDPadding     string
	palette          bool
	releasePlusRules BZRule
)

func todoList(args []string) ([]*gitlab.Todo, error) {
	num, err := strconv.Atoi(todoNumRet)
	if todoAll || (err != nil) {
		num = -1
	}

	opts := gitlab.ListTodosOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: num,
		},
	}

	var lstr = strings.ToLower(todoType)
	switch {
	case lstr == "mr":
		targetType = "MergeRequest"
		opts.Type = &targetType
	case lstr == "issue":
		targetType = "Issue"
		opts.Type = &targetType
	}

	return lab.TodoList(opts, num)
}

func containsDate(holidays []time.Time, holiday time.Time) bool {
	for _, date := range holidays {
		if date == holiday {
			return true
		}
	}
	return false
}

func containsUserName(names []*gitlab.BasicUser, name string) bool {
	for _, a := range names {
		if a.Username == name {
			return true
		}
	}
	return false
}

func contains(names []string, name string) bool {
	for _, a := range names {
		if a == name {
			return true
		}
	}
	return false
}

func match(names []string, name string) int {
	for count, a := range names {
		if a == name {
			return count
		}
	}
	return -1
}

func common(a []string, b []string) (ret []string) {
	com := make(map[string]bool)

	for _, item := range a {
		com[item] = true
	}

	for _, item := range b {
		if _, ok := com[item]; ok {
			ret = append(ret, item)
		}
	}

	return ret
}

func truncateString(str string, num int) string {
	newStr := str
	if len(str) > num {
		if num > 3 {
			num -= 3
		}
		newStr = str[0:num] + "..."
	}
	return newStr
}

func getProjectTodos(args []string) (projectTodos []*gitlab.Todo) {
	if project != "" {
		fmt.Printf("Fetching %s Todos for %s...\n", username, project)
	}

	// create a list of Todos for THIS project
	todoAll = true
	todoType = "mr"
	todos, err := todoList(args)
	if err != nil {
		log.Fatal(err)
	}

	var (
		nameTodos  []string
		countTodos []int
	)

	for _, todo := range todos {
		index := match(nameTodos, todo.Project.Name)
		if index == -1 {
			nameTodos = append(nameTodos, todo.Project.Name)
			countTodos = append(countTodos, 0)
			index = len(nameTodos) - 1
		}
		countTodos[index]++

		if project != "" && todo.Project.PathWithNamespace == project && todo.TargetType == "MergeRequest" {
			projectTodos = append(projectTodos, todo)
		}
	}

	for n, name := range nameTodos {
		fmt.Printf("%s has %d Todos() in %s\n", username, countTodos[n], name)
	}

	return projectTodos
}

func getThisTodo(mrIID int, projectTodos []*gitlab.Todo) *gitlab.Todo {
	for _, todo := range projectTodos {
		if mrIID == todo.Target.IID {
			return todo
		}
	}
	return nil
}

func getBZIDsFromDescription(description string) ([]string, error) {
	var bzStrs []string

	bzExp, _ := regexp.Compile("Bugzilla:.*")
	_bzStrs := bzExp.FindAllString(description, -1)
	if len(_bzStrs) == 0 {
		return nil, fmt.Errorf("No Bugzilla: found in the MR description.")
	}

	for _, bzStr := range _bzStrs {
		bzStr = strings.TrimSpace(bzStr)

		// It doesn't make sense to have an INTERNAL BZ with a normal
		// BZ.  Authors would just use the normal BZ.
		if strings.Contains(bzStr, "INTERNAL") {
			bzStrs = append(bzStrs, "INTERNAL")
			return bzStrs, nil
		}

		// Either of these are valid strings:
		// Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=1837350
		// Bugzilla: https://bugzilla.redhat.com/1837350
		if strings.Contains(bzStr, "=") {
			s := strings.Split(bzStr, "=")
			_, err := strconv.Atoi(s[1])
			if err != nil {
				return nil, fmt.Errorf("Invalid bugzilla entry")
			}
			bzStrs = append(bzStrs, s[1])
			continue
		}

		s := strings.Split(bzStr, "/")
		_bzStr := s[len(s)-1]
		_, err := strconv.Atoi(_bzStr)
		if err != nil {
			return nil, fmt.Errorf("Invalid bugzilla entry")
		}
		bzStrs = append(bzStrs, _bzStr)
	}

	return bzStrs, nil
}

func getUsersSubsystemLabels() []string {
	fmt.Printf("Fetching %s labels for %s from owners.yaml...\n", username, project)
	ownersLabels, err := getOwnersLabels()
	if err != nil {
		log.Fatal(err)
	}

	if username != lab.User() {
		// cannot get remaining information for other users
		return ownersLabels
	}
	combinedLabels := ownersLabels

	fmt.Printf("Fetching %s labels for %s from GitLab...\n", username, project)
	var subscribedGitLabLabels []string
	var subscribedLabels []string
	// Cannot rely on GitLab built-in label support because of:
	// https://gitlab.com/gitlab-com/account-management/eastern-north-america/red-hat/red-hat-rhel-group-pov/-/issues/72
	gitLablabels, err := lab.LabelList(project)
	if err != nil {
		log.Fatal(err)
	}
	for _, label := range gitLablabels {
		if label.Subscribed {
			subscribedGitLabLabels = append(subscribedGitLabLabels, label.Name)
		}
	}

	// combine the labels in owners.yaml with the self-chosen ones in
	// the config and remove whitespace, and the ones reported by GitLab as
	// "subscribed"
	fmt.Printf("Fetching %s labels for %s from config...\n", username, project)
	combinedLabels = append(combinedLabels, config.GetSubscribedLabels()...)
	combinedLabels = append(combinedLabels, subscribedGitLabLabels...)

	// remove whitespaces from labels
	for count, label := range combinedLabels {
		combinedLabels[count] = strings.TrimSpace(label)
	}

	// Create a map of all unique elements.
	encountered := map[string]bool{}
	for v := range combinedLabels {
		encountered[combinedLabels[v]] = true
	}

	// This is the final list of labels from owners.yaml and subscribed labels
	for key, _ := range encountered {
		subscribedLabels = append(subscribedLabels, key)
	}

	var subsystemLabels []string
	for _, label := range subscribedLabels {
		label = strings.TrimSpace(label)
		if strings.Contains(label, "Subsystem") || label == "KABI" {
			subsystemLabels = append(subsystemLabels, label)
		}
	}

	return subsystemLabels
}

func evaluateFlag(name string, value string, states []string, indent string) {
	for _, state := range states {
		if state == value {
			return
		}
	}
	fmt.Printf(red("%s - Bugzilla has invalid %s flag (%s)\n"), indent, name, value)
}

func verifyPackageRules(bzStr string) (falseEvals []string) {
	bzData := getBZdata(bzStr)

	ruleName := ""
	if bzData.b["internal_target_release"] != "Not Set" {
		ruleName = "rhel-" + bzData.b["internal_target_release"]
	} else {
		ruleName = "rhel-" + bzData.b["zstream_target_release"]
	}

	// get the Package Rule
	infixString := getPkgsDevelRule(ruleName)
	// convert to PostFix
	postFix := postfix.ToLogicalPostfix(infixString)
	// remove Jira entries
	display, variables := removeJiraEntries(postFix, false)
	final := display
	for _, v := range variables {
		r, _ := regexp.Compile("\\b" + v + "\\b")
		final = r.ReplaceAllLiteralString(final, bzData.b[v])
	}
	// verify the rule
	_, falseEvals = rulesAreValid(final, display)

	return falseEvals
}

func decipherAuthorLabels(mrIID string, description string, labels []string) {
	if contains(labels, "readyForQA") {
		bzStrs, err := getBZIDsFromDescription(description)
		if err != nil {
			fmt.Printf(red("%s %s - status: Invalid Bugzilla: entry in MR Description.  Fix the MR description.\n"), noRole, mrIIDPadding)
			fmt.Printf(red("%s %s   ex) lab mr edit %s\n"), noRole, mrIIDPadding, mrIID)
			return
		}

		for _, bzStr := range bzStrs {
			QAname, QAemail, OtherQA := getBugzillaQAContact(bzStr)
			color := green

			fmt.Printf(color("%s %s - status: readyForQA (waiting for QA to test artifacts.  MR is not merged.)\n"), noRole, mrIIDPadding)
			fmt.Printf(color("%s %s           - https://bugzilla.redhat.com/%s\n"), noRole, mrIIDPadding, bzStr)
			if OtherQA {
				fmt.Printf(color("%s %s           - QA contact: %s <%s> (OtherQA)\n"), noRole, mrIIDPadding, QAname, QAemail)
			} else {
				fmt.Printf(color("%s %s           - QA contact: %s <%s>\n"), noRole, mrIIDPadding, QAname, QAemail)
			}
		}
		return
	} else if contains(labels, "readyForMerge") {
		fmt.Printf(green("%s %s - status: readyForMerge (waiting for maintainer to merge)\n"), noRole, mrIIDPadding)
		return
	}

	// output labels
	fmt.Printf(red("%s %s - labels: "), noRole, mrIIDPadding)
	labelPadding := termWidth - len(noRole) - len(mrIIDPadding) - 12

	comma := green(", ")
	remainingPadding := labelPadding
	for cnt, label := range labels {
		if cnt != 0 {
			fmt.Printf(comma)
		}
		// the -2 at the end is for the commas
		remainingPadding = remainingPadding - len(label) - 2
		if remainingPadding <= 0 {
			fmt.Printf("\n%s", strings.Repeat(" ", termWidth-labelPadding))
			// the -2 at the end is for the commas
			remainingPadding = labelPadding - len(label) - 2
		}
		if strings.Contains(label, "::OK") {
			fmt.Printf("%s", green(label))
			comma = green(", ")
		} else if strings.Contains(label, "Subsystem:") {
			fmt.Printf("%s", grey(label))
			comma = grey(", ")
		} else if strings.Contains(label, "Bugzilla::NeedsTesting") {
			fmt.Printf("%s", green(label))
			comma = green(", ")
		} else {
			fmt.Printf("%s", red(label))
			comma = red(", ")
		}
	}
	fmt.Printf("\n")

	if contains(labels, "Acks::NeedsReview") {
		fmt.Printf(red("%s %s - status: Acks::NeedsReview: This MR needs more technical reviews.  An easy way \n"), noRole, mrIIDPadding)
		fmt.Printf(red("%s %s           to determine reviewers is to remove the \"Acks::NeedsReview\" label.\n"), noRole, mrIIDPadding)
		fmt.Printf(red("%s %s           ex) lab mr edit %s "), noRole, mrIIDPadding, mrIID)
		fmt.Printf(red("--unlabel Acks::NeedsReview\n"))
	}

	bzStrs, err := getBZIDsFromDescription(description)
	if err != nil {
		fmt.Printf(red("%s %s - status: Invalid Bugzilla: entry in MR Description.  Fix the MR description.\n"), noRole, mrIIDPadding)
		fmt.Printf(red("%s %s           ex) lab mr edit %s\n"), noRole, mrIIDPadding, mrIID)
		return
	}

	bugzillaSpc := "           " + noRole + mrIIDPadding

	// Bugzilla::NeedsReview is very complicated because when the label
	// is applies to an MR it means that the Bugzilla itself must be analyzed.
	if contains(labels, "Bugzilla::NeedsReview") {
		for _, bzStr := range bzStrs {
			// INTERNAL BZs have no "real" BZ.  I'm not sure that
			// Bugzilla::NeedsReview can be applied to an INTERNAL BZ.
			if strings.Contains(bzStr, "INTERNAL") {
				fmt.Printf(red("%s %s - status: Bugzilla::NeedsReview: INTERNAL BZ specified\n"), noRole, mrIIDPadding)
				continue
			}

			longBZStr := fmt.Sprintf("https://bugzilla.redhat.com/%s", bzStr)

			bzData := getBZdata(bzStr)

			// First check the bugzilla release flag status, and if that is "+"
			// only then evaluate the packaging rules (in the else statement below)
			if bzData.b["release"] != "+" {
				fmt.Printf(red("%s %s - status: Bugzilla::NeedsReview: The bugzilla flags are not set correctly.\n"), noRole, mrIIDPadding)
				fmt.Printf(red("%s See the BZ for details: %s\n"), bugzillaSpc, longBZStr)

				// get the rules for setting release+ only once
				if len(releasePlusRules.Result) == 0 {
					// The order of these is reflected in the ruleIndex below.
					// Yes, I could do a search on name but it would just
					// slow things down.
					rulesNames := []string{"RHEL SySc Approve 36ITM (release+ flag)", "RHEL SySc Approve (release flag when zstream)", "RHEL SySc Post-Beta Approve with exception+ (release flag)", "RHEL SySc Post-Beta Approve with blocker+ (release flag)"}
					releasePlusRules = getBZReleasePlusRules(rulesNames)
				}
				ruleIndex := 0 // default to ystream

				// There is a possible state where both the ITR and ZTR are
				// set when a BZ has been proposed for z-stream.  In this
				// case the BZ is still a ystream BZ, and not targeted for
				// zstream.  A zstream BZ will be cloned from the regular
				// BZ by the maintainers.
				if bzData.b["internal_target_release"] != "Not Set" {
					itm, err := strconv.Atoi(bzData.b["internal_target_milestone"])
					if err != nil {
						// if the ITM is unset, this can be a zstream
						if bzData.b["zstream_target_release"] != "Not Set" {
							ruleIndex = 1
						} else {
							// if the ITR, ZTR, and ITM aren't set the
							// assume the user is doing a ystream case
							ruleIndex = 0
						}
					} else {
						if itm > 26 {
							if bzData.b["exception"] != "Not Found" {
								ruleIndex = 2
							} else if bzData.b["blocker"] != "Not Found" {
								ruleIndex = 3
							} else {
								// post ITM 26 no exception or blocker is an error
								fmt.Printf(red("%s - Bugzilla is post-ITM 26 and does not have blocker or exception requested\n"), bugzillaSpc)
								// assume blocker is going to be requested
								ruleIndex = 3
							}
						} else {
							// ystream
							ruleIndex = 0
						}
					}
				} else if bzData.b["zstream_target_release"] != "Not Set" {
					ruleIndex = 1
				}
				rule := releasePlusRules.Result[ruleIndex].Details.MatchJson

				//
				// All evaluations below this line are based on the rule
				//

				// Status
				if !contains(rule.BugStatus.Values, bzData.b["status"]) {
					fmt.Printf(red("%s - Bugzilla has invalid Status (%s)\n"), bugzillaSpc, bzData.b["status"])
				}

				// Product
				if !contains(rule.Product.Values, bzData.b["product"]) {
					fmt.Printf(red("%s - Bugzilla has invalid Product (%s)\n"), bugzillaSpc, bzData.b["product"])
				}

				// ITM: Internal Target Milestone
				isRuleITM := contains(rule.InternalTargetMilestone.Values, bzData.b["internal_target_milestone"])
				switch {
				case rule.InternalTargetMilestone.Word == "is any of":
					if !isRuleITM {
						fmt.Printf(red("%s - Bugzilla has invalid ITM (%s)\n"), bugzillaSpc, bzData.b["internal_target_milestone"])
					}
				case rule.InternalTargetMilestone.Word == "is not any of":
					if isRuleITM {
						fmt.Printf(red("%s - Bugzilla has invalid ITM (%s)\n"), bugzillaSpc, bzData.b["internal_target_milestone"])
					}
				default:
					log.Fatalf("Bugzilla rule has invalid ITM word (%s)\n", rule.InternalTargetMilestone.Word)
				}

				// ITR: Internal Target Release
				isRuleITR := contains(rule.InternalTargetRelease.Values, bzData.b["internal_target_release"])
				switch {
				case rule.InternalTargetRelease.Word == "is any of":
					if !isRuleITR {
						fmt.Printf(red("%s - Bugzilla has invalid ITR (%s)\n"), bugzillaSpc, bzData.b["internal_target_release"])
					}
				case rule.InternalTargetRelease.Word == "is not any of":
					if isRuleITR {
						fmt.Printf(red("%s - Bugzilla has invalid ITR (%s)\n"), bugzillaSpc, bzData.b["internal_target_release"])
					}
				default:
					log.Fatalf("Bugzilla rule has invalid ITR word (%s)\n", rule.InternalTargetRelease.Word)
				}

				//
				// Flags
				//
				for _, flag := range rule.FlagTypes {
					if flag.Name == "release" {
						// This was done above to determine that
						// the BZ is in the wrong state.  Nothing to do
						// here.
						continue
					}
					evaluateFlag(flag.Name, bzData.b[flag.Name], flag.Status, bugzillaSpc)
				}

				// release+ can be stripped when the ITM or devTM missed.
				// If the BZ is pre-MODIFIED the deadline represents the devTM
				// deadline.  After MODIFIED, the deadline represents the ITM
				// deadline.
				if bzData.b["internal_target_milestone"] != "Not Set" && bzData.b["status"] != "NEW" && bzData.b["status"] != "ASSIGNED" && bzData.b["status"] != "POST" {
					// bzData.deadline reflects ITM
					if bzData.deadline.Before(time.Now()) {
						fmt.Printf(red("%s - Bugzilla: %s ", bugzillaSpc, longBZStr))
						fmt.Printf(red("has missed ITM %s (deadline was %s)\n"), bzData.b["internal_target_milestone"], bzData.deadline.Format("2006-01-02"))
					}
				} else if bzData.b["dev_target_milestone"] != "Not Set" && bzData.b["status"] == "NEW" && bzData.b["status"] == "ASSIGNED" && bzData.b["status"] == "POST" {
					// bzData.deadline reflects devTM
					if bzData.deadline.Before(time.Now()) {
						fmt.Printf(red("%s - Bugzilla: %s ", bugzillaSpc, longBZStr))
						fmt.Printf(red("has missed devTM %s (deadline was %s)\n"), bzData.b["dev_target_milestone"], bzData.deadline.Format("2006-01-02"))
					}
				}
			} else {
				fmt.Printf(red("%s %s - status: Bugzilla::NeedsReview: %s\n"), noRole, mrIIDPadding, longBZStr)
				fmt.Printf(red("%s %s           One of your commits is missing a Bugzilla: entry.  See MR %s.\n"), noRole, mrIIDPadding, mrIID)
				fmt.Printf(red("%s %s           ex) lab mr show %s "), noRole, mrIIDPadding, mrIID)
				fmt.Printf(red("--comments\n"))
			}
		}
	} else {
		for _, bzStr := range bzStrs {
			if strings.Contains(bzStr, "INTERNAL") {
				fmt.Printf(red("%s %s - status: Bugzilla::NeedsReview: INTERNAL BZ specified\n"), noRole, mrIIDPadding)
				continue
			}
			for e, eval := range verifyPackageRules(bzStr) {
				if e == 0 {
					fmt.Printf(red("%s %s - Bugzilla: https://bugzilla.redhat.com/%s\n"), noRole, mrIIDPadding, bzStr)
				}
				fmt.Printf(red("%s - Packaging rule requires %s\n"), bugzillaSpc, eval)
			}
		}
	}

	if contains(labels, "Bugzilla::NeedsTesting") {
		for _, bzStr := range bzStrs {
			if strings.Contains(bzStr, "INTERNAL") {
				fmt.Printf(red("%s %s - status: Bugzilla::NeedsReview: INTERNAL BZ specified\n"), noRole, mrIIDPadding)
				continue
			}
			longBZStr := fmt.Sprintf("https://bugzilla.redhat.com/%s", bzStr)
			color := green

			fmt.Printf(green("%s %s - status: Bugzilla::NeedsTesting: The bugzilla's changes are waiting to be tested.\n"), noRole, mrIIDPadding)
			fmt.Printf(green("%s %s           See the BZ for details: %s\n"), noRole, mrIIDPadding, longBZStr)

			QAname, QAemail, OtherQA := getBugzillaQAContact(bzStr)

			if OtherQA {
				fmt.Printf(color("%s %s           - QA contact: %s <%s> (OtherQA)\n"), noRole, mrIIDPadding, QAname, QAemail)
			} else {
				fmt.Printf(color("%s %s           - QA contact: %s <%s>\n"), noRole, mrIIDPadding, QAname, QAemail)
			}
		}
	}

	if contains(labels, "CommitRefs::NeedsReview") {
		fmt.Printf(red("%s %s - status: CommitRefs::NeedsReview: The commits have not passed automated review.\n"), noRole, mrIIDPadding)
		fmt.Printf(red("%s %s           Review the \"Upstream Commit ID Readiness\" report.\n"), noRole, mrIIDPadding)
		fmt.Printf(red("%s %s           ex) lab mr show %s "), noRole, mrIIDPadding, mrIID)
		fmt.Printf(red("--comments\n"))
	}
	if contains(labels, "Signoff::NeedsReview") {
		fmt.Printf(red("%s %s - status: Signoff::NeedsReview: The commits have not passed automated review.\n"), noRole, mrIIDPadding)
		fmt.Printf(red("%s %s           Review the \"DCO Signoff Error(s)!\" report.\n"), noRole, mrIIDPadding)
		fmt.Printf(red("%s %s           ex) lab mr show %s "), noRole, mrIIDPadding, mrIID)
		fmt.Printf(red("--comments\n"))
	}
	if contains(labels, "CKI::NeedsReview") {
		fmt.Printf(red("%s %s - status: Review CKI results in MR %s"), noRole, mrIIDPadding, mrIID)
		fmt.Printf(red(".  Debugging information can be found at:\n"))
		fmt.Printf(red("%s %s           https://cki-project.org/docs/user_docs/gitlab-mr-testing/full_picture/#debugging-and-fixing-failures---more-details\n"), noRole, mrIIDPadding)
	}
	if contains(labels, "CKI::Running") {
		fmt.Printf(red("%s %s - status: CKI has not completed for MR %s\n"), noRole, mrIIDPadding, mrIID)
	}

	if contains(labels, "Acks::Blocked") {
		fmt.Printf(red("%s %s - status: Acks::Blocked: The changes have been blocked by reviewer(s) in a thread.\n"), noRole, mrIIDPadding)
		fmt.Printf(red("%s %s           See the MR for details.\n"), noRole, mrIIDPadding)
		fmt.Printf(red("%s %s           ex) lab mr show %s --with-comments\n"), noRole, mrIIDPadding, mrIID)
	}

	if contains(labels, "Acks::NACKed") {
		fmt.Printf(red("%s %s - status: Acks::Nacked: The changes have been rejected by reviewer(s).\n"), noRole, mrIIDPadding)
		fmt.Printf(red("%s %s           See the MR for details.\n"), noRole, mrIIDPadding)
		fmt.Printf(red("%s %s           ex) lab mr show %s --with-comments\n"), noRole, mrIIDPadding, mrIID)
	}
}

func addLabel(label string) {
	labels := config.GetSubscribedLabels()
	if contains(labels, label) {
		fmt.Printf("Labels already contain %s ... no change made.\n", label)
		return
	}
	labels = append(labels, label)
	newlabels := strings.Join(labels, ",")

	config.MainConfig.Set("core.subscribedlabels", newlabels)
	config.MainConfig.WriteConfig()

	fmt.Printf("Labels set as: %s\n", newlabels)
}

func removeLabel(label string) {
	labels := config.GetSubscribedLabels()
	if !contains(labels, label) {
		fmt.Printf("Labels do not contain %s ... no change made.\n", label)
		return
	}

	newlabels := ""
	comma := false
	for _, _label := range labels {
		if _label == label {
			continue
		}
		if comma == true {
			newlabels = newlabels + ","
		}
		newlabels = newlabels + _label
		comma = true
	}

	config.MainConfig.Set("core.subscribedlabels", newlabels)
	config.MainConfig.WriteConfig()

	fmt.Println("Labels set as:", newlabels)
}

// getAckStartDate() returns the date that the MR Required Approval timeout
// starts at, and the date that the MR was assigned to the user.
// It will look for the last time the MR was placed into the Ready state, and if that
// doesn't exist, it returns date that the MR was added as a Todo item, or
// finally date that the MR was created.
//
// Readers might ask the question "What if someone adds me as a reviewer?" and realize
// this code doesn't take that into account.  That's correct -- it doesn't.  If you are
// manually added by an author or other participant, you are NOT a required approver.
func getAckStartDate(mr *gitlab.MergeRequest, discussions []*gitlab.Discussion, todo *gitlab.Todo) (*time.Time, *time.Time, []*gitlab.Discussion) {

	var ackDate *time.Time
	var assignDate *time.Time

	_assignDate := time.Now()
	_assignDate = _assignDate.AddDate(1, 0, 0)
	assignDate = &_assignDate

	if discussions == nil {
		var err error
		discussions, err = lab.MRListDiscussions(project, mr.IID)
		if err != nil {
			log.Fatal(err)
		}
	}
	// Find the last date that the MR was marked as Ready, and the last date
	// that the user was assigned to the MR
	for _, discussion := range discussions {
		for _, note := range discussion.Notes {
			if note.System {
				if note.Body == "marked this merge request as **ready**" {
					ackDate = note.UpdatedAt
				}
				if note.Body == ("assigned to @" + username) {
					assignDate = note.UpdatedAt
				}
			}
		}
	}
	if ackDate != nil {
		return ackDate, assignDate, discussions
	}

	// Return the date that the MR was added as a Todo item
	if todo != nil {
		return todo.CreatedAt, assignDate, discussions
	}
	// If that doesn't exist, return the date that the MR was created
	return mr.CreatedAt, assignDate, discussions
}

func getAckEndDate(mr *gitlab.MergeRequest, discussions []*gitlab.Discussion, todo *gitlab.Todo) (bool, int, time.Time, []*gitlab.Discussion) {
	var (
		ackStartDate *time.Time
		assignDate   *time.Time
	)

	holidays := config.GetHolidays()
	ackStartDate, assignDate, discussions = getAckStartDate(mr, discussions, todo)
	ackEndDate := ackStartDate.Round(24 * time.Hour)
	daysAdded := 0
	days := 0
	for {
		days++
		ackEndDate = ackEndDate.Add(time.Hour * 24)
		weekday := ackEndDate.Weekday().String()
		if weekday == "Sunday" || weekday == "Saturday" {
			continue
		}
		if containsDate(holidays, ackEndDate) {
			continue
		}
		daysAdded++
		if daysAdded == 10 {
			if assignDate.After(ackEndDate) {
				return true, int((time.Until(ackEndDate)).Hours()) / 24, ackEndDate, discussions
			}
			break
		}
	}
	return false, -1, ackEndDate, discussions
}

func isMRBlocked(mr *gitlab.MergeRequest, discussions []*gitlab.Discussion) (int, string, []*gitlab.Discussion) {
	if mr.BlockingDiscussionsResolved {
		return 0, "", discussions
	}

	if discussions == nil {
		var err error
		discussions, err = lab.MRListDiscussions(project, mr.IID)
		if err != nil {
			log.Fatal(err)
		}
	}

	// There are two types of blocking threads.  The first is a standalone
	// created by someone in the MR.  In that case the UpdatedAt and
	// CreatedAt times should be the same.  In the case of where someone
	// replies to a comment and creates a blocking thread, the CreatedAt
	// and UpdatedAt times will be different as the note's UpdatedAt time
	// will be updated to the newly created blocking comment's CreatedAt
	// date.   Hence, the complexity below...
	var lastNote *gitlab.Note
	for _, discussion := range discussions {
		subBlockingThread := false
		for _, note := range discussion.Notes {
			if note.Type == "DiscussionNote" && note.Resolvable == true && note.Resolved == false {
				if subBlockingThread == false && time.Time(*note.UpdatedAt).Equal(time.Time(*note.CreatedAt)) {
					return note.ID, note.Author.Username, discussions
				}
				if subBlockingThread == true {
					return lastNote.ID, note.Author.Username, discussions
				}
				subBlockingThread = true
			}
			lastNote = note
		}
	}
	return 0, "", discussions
}

// reviewCmd represents the list command
var reviewCmd = &cobra.Command{
	Use:   "rhstatus [mrID]",
	Short: "RedHat merge request status",
	Long:  ``,
	Args:  cobra.MaximumNArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		if palette {
			showPalette()
			return
		}
		setPalette()

		if addlabel != "" {
			addLabel(addlabel)
			return
		}
		if removelabel != "" {
			removeLabel(removelabel)
			return
		}

		_project, mrID, err := parseArgsRemoteAndID(args)
		if err != nil {
			return
		}
		project = _project

		var projectTodos []*gitlab.Todo
		if username == "" {
			username = lab.User()
			projectTodos = getProjectTodos(args)
		}

		if project == "" {
			// this "empty" return allows rhstatus to return the
			// Todo list in a non-GitLab dir.
			return
		}

		subsystemLabels := getUsersSubsystemLabels()

		// Get Merge Request data
		var mrs []*gitlab.MergeRequest
		fmt.Printf("Fetching %s Merge Requests for %s...\n", username, project)
		if mrID == 0 {
			mrAll = true
			mrs, err = mrList(args)
			if err != nil {
				log.Fatal(err)
			}
		} else {
			mr, err := lab.MRGet(project, int(mrID))
			if err != nil {
				log.Fatal(err)
			}
			mrs = append(mrs, mr)
		}

		pager := NewPager(cmd.Flags())
		defer pager.Close()

		maxMR := 0
		for _, mr := range mrs {
			if mr.IID > maxMR {
				maxMR = mr.IID
			}
		}
		mrPadding = len(strconv.Itoa(maxMR))
		mrIIDPadding = strings.Repeat(" ", mrPadding)
		// This is used to calculate the displayed title with ellipses.
		// The "3" at the end is the number of spaces between columns
		// (ie, there are 3 columns with 2 spaces), and an extra -1 for
		// the EOL
		mrTitleTruncateLength := termWidth - len(noRole) - len(mrIIDPadding) - 3

		for _, mr := range mrs {
			var (
				approvers            []string
				blocked              int
				commonLabels         []string
				discussions          []*gitlab.Discussion
				role                 string = noRole
				roleColor            func(...interface{}) string
				roleDefined          bool = false
				isAuthor             bool = false
				isReviewer           bool = false
				isAssignee           bool = false
				isReqApprover        bool = false
				isApprovedByUser     bool = false
				unapprovedSubsystems []string
				blockedByUser        string
			)

			thisTodo := getThisTodo(mr.IID, projectTodos)

			mrIID := strconv.Itoa(mr.IID)

			title := truncateString(mr.Title, mrTitleTruncateLength)

			if !fullmode {
				// Standard mode ONLY reports detailed status
				// for MRs that the user has a role.  ie) user
				// is a reviewer, an assignee, or an author.
				// Standard mode assumes that the ACK/NACK bot
				// has done it's magic and added a user to the
				// MR correctly.
				if !containsUserName(mr.Assignees, username) && !containsUserName(mr.Reviewers, username) && mr.Author.Username != username && thisTodo == nil {
					if interest {
						continue
					}
					// check for labels
					if mr.WorkInProgress {
						fmt.Printf("%s %s%s %s \n", role, strings.Repeat(" ", mrPadding-len(mrIID)), grey(mrIID), grey(title))
					} else {
						fmt.Printf("%s %s%s %s \n", role, strings.Repeat(" ", mrPadding-len(mrIID)), mrIID, title)
					}
					continue
				}
			}

			// check to see if the user is the author of the MR
			if mr.Author.Username == username {
				isAuthor = true
				roleDefined = true
			}

			if !isAuthor {
				if containsUserName(mr.Assignees, username) {
					isAssignee = true
					roleDefined = true
				}

				if containsUserName(mr.Reviewers, username) {
					isReviewer = true
					roleDefined = true
				}
			}

			// The rule examination below doesn't take into account
			// approvals done on MRs that a user has no role in.
			approvalConfig, err := lab.GetMRApprovalsConfiguration(project, mr.IID)
			if err != nil {
				log.Fatal(err)
			}
			for _, approvedby := range approvalConfig.ApprovedBy {
				approvers = append(approvers, approvedby.User.Username)
				if approvedby.User.Username == username {
					roleDefined = true
					isApprovedByUser = true
				}
			}

			// if user is the author then they can't be a required approver
			if !isAuthor {
				approvals, err := lab.GetMRApprovalState(project, mr.IID)
				if err != nil {
					log.Fatal(err)
				}

				for _, rule := range approvals.Rules {
					for _, eligible := range rule.EligibleApprovers {
						if eligible.Username == username {
							roleDefined = true
							isReqApprover = true
							if !rule.Approved {
								unapprovedSubsystems = append(unapprovedSubsystems, rule.Name)
							}
						}
					}
				}
			}

			// Leave these lines for easier debug
			//fmt.Println("isAuthor", "isAssignee", "isReviewer", "isReqApprover", "isApprovedByUser", "len(unapprovedSubsystems)")
			//fmt.Println(isAuthor, isAssignee, isReviewer, isReqApprover, isApprovedByUser, len(unapprovedSubsystems))

			roleColor = green
			if roleDefined {
				if isAuthor {
					roleColor = yellow
					if mr.HasConflicts || !mr.BlockingDiscussionsResolved {
						roleColor = red
					}
					role = "author  "
				} else if isApprovedByUser {
					roleColor = green
					role = "approved"
				} else if isReviewer && !isReqApprover {
					roleColor = green
					role = "reviewer"
					// if the user has added their own Todo, mark it yellow
					if thisTodo != nil && thisTodo.Author.Username == lab.User() {
						roleColor = yellow
						role = "reviewer"
					}
				} else if isReqApprover && len(unapprovedSubsystems) != 0 {
					roleColor = red
					role = "approver"
				} else if isReqApprover && len(unapprovedSubsystems) == 0 {
					roleColor = green
					role = "approver"
				} else if isAssignee {
					roleColor = green
					role = "assignee"
					// if the user has added their own Todo, mark it yellow
					if thisTodo != nil && thisTodo.Author.Username == lab.User() {
						roleColor = yellow
						role = "assignee"
					}
				}
			} else {
				// check to see if this MR's labels match the user's labels
				commonLabels = common(subsystemLabels, mr.Labels)
				if commonLabels != nil {
					// This is considered an Unassigned state because
					// the user is not a author, reviewer, or assignee.
					// It should be highlighted
					roleColor = green
					role = "labels  "
				}
			}

			// check to see if this MR is on the user's Todo list
			mrIIDColor := white
			if thisTodo != nil {
				if !roleDefined {
					// todo list isn't a real role
					roleColor = green
					role = "todo    "
				}
				mrIIDColor = BLUE
			}

			// if this MR is a draft, then grey it out
			if mr.WorkInProgress {
				title = grey(title)
				if thisTodo == nil {
					mrIIDColor = grey
				}
			}

			blocked, blockedByUser, discussions = isMRBlocked(mr, discussions)
			if blockedByUser == username {
				roleColor = red
			}

			// output basic information (status, MR, and Title)
			fmt.Printf("%s %s%s %s\n", roleColor(role), strings.Repeat(" ", mrPadding-len(mrIID)), mrIIDColor(mrIID), title)

			// List MR labels for MRs that the user is interested in.
			// This only happens if the user does not have a role on
			// the MR (see above)
			if !roleDefined && commonLabels != nil {
				fmt.Printf(red("%s %s - MR %s "), noRole, mrIIDPadding, mrIID)
				fmt.Printf(red("has labels: "))
				for cnt, label := range commonLabels {
					if cnt != 0 {
						fmt.Printf(red(", "))
					}
					fmt.Printf(red("%s"), label)
				}
				fmt.Printf("\n")
				fmt.Printf(red("%s %s   If you want to review update the MR with:\n"), noRole, mrIIDPadding)
				fmt.Printf(red("%s %s   ex) lab mr edit %s "), noRole, mrIIDPadding, mrIID)
				fmt.Printf(red("--review %s\n"), username)
			}

			// list any existing approvers
			if roleDefined && approvers != nil && !isApprovedByUser && !mr.WorkInProgress {
				fmt.Printf("%s %s - approved by: ", noRole, mrIIDPadding)
				for cnt, approver := range approvers {
					if cnt != 0 {
						fmt.Printf(", ")
					}
					fmt.Printf("%s", approver)
				}
				fmt.Printf("\n")
			}

			// if the user is the author, output additional information
			// about the labels
			if isAuthor {
				decipherAuthorLabels(mrIID, mr.Description, mr.Labels)
				if mr.HasConflicts {
					fmt.Printf(red("%s %s - MR %s has conflicts and needs to be rebased\n"), noRole, mrIIDPadding, mrIID)
				}
			}

			if blocked > 0 {
				fmt.Printf(red("%s %s - MR %s "), noRole, mrIIDPadding, mrIID)
				fmt.Printf(red("is blocked by %s and needs to be resolved\n"), blockedByUser)
				if blockedByUser == username {
					fmt.Printf(red("%s %s   After review the MR can be resolved with\n"), noRole, mrIIDPadding)
					fmt.Printf(red("%s %s   ex) lab mr reply %s"), noRole, mrIIDPadding, mrIID)
					fmt.Printf(red(":%d "), blocked)
					fmt.Printf(red("--resolve\n"))
				}
			}

			// calculate the Acknowledgement Deadline which is 10 business
			// days from notification
			if !isAuthor && roleDefined && !isApprovedByUser && !mr.WorkInProgress {
				var (
					output     bool
					daysLeft   int
					ackEndDate time.Time
				)
				output, daysLeft, ackEndDate, discussions = getAckEndDate(mr, discussions, thisTodo)
				if !output {
					continue
				}

				if daysLeft < 0 {
					fmt.Printf(red("%s %s - status: Acknowledgement date was missed (%s)\n"), noRole, mrIIDPadding, ackEndDate.Format("2006-01-02"))
				} else {
					fmt.Printf(red("%s %s - status: Acknowledgement Deadline days left: %d (%s)\n"), noRole, mrIIDPadding, daysLeft, ackEndDate.Format("2006-01-02"))
				}
			}

			// Check to see if this MR can be removed from the Todo list
			// because the user does not have a role
			if thisTodo != nil && !roleDefined {
				if thisTodo.Author.Username != username {
					fmt.Printf("%s %s - MR %s seems to no longer need your attention and may be removed\n", noRole, mrIIDPadding, mrIID)
					fmt.Printf("%s %s   from your Todo list.\n", noRole, mrIIDPadding)
					fmt.Printf("%s %s   ex) lab todo done %d \n", noRole, mrIIDPadding, thisTodo.ID)
				}
			}

			// Check to see if this MR can be removed from the Todo list
			// because the user has approved the MR
			if thisTodo != nil && isApprovedByUser {
				fmt.Printf("%s %s - MR %s is approved and can be removed from your Todo List.\n", noRole, mrIIDPadding, mrIID)
				fmt.Printf("%s %s   ex) lab todo done %d \n", noRole, mrIIDPadding, thisTodo.ID)
			}
		}
	},
}

func init() {
	reviewCmd.Flags().StringVar(&mrAuthor, "author", "", "list only MRs authored by $username")
	reviewCmd.Flags().StringVar(
		&mrAssignee, "assignee", "", "list only MRs assigned to $username")
	reviewCmd.Flags().StringVar(&mrOrder, "order", "updated_at", "display order (updated_at/created_at)")
	reviewCmd.Flags().StringVar(&mrSortedBy, "sort", "desc", "sort order (desc/asc)")
	reviewCmd.Flags().BoolVarP(&mrDraft, "draft", "", false, "list MRs marked as draft")
	reviewCmd.Flags().BoolVarP(&mrReady, "ready", "", false, "list MRs not marked as draft")
	reviewCmd.Flags().SortFlags = false
	reviewCmd.Flags().StringVar(
		&mrReviewer, "reviewer", "", "list only MRs with reviewer set to $username")
	reviewCmd.Flags().StringVar(&addlabel, "add-label", "", "Add a label to rhstatus' watch list")
	reviewCmd.Flags().StringVar(&removelabel, "remove-label", "", "Remove a label from rhstatus' watch list")
	reviewCmd.Flags().BoolVarP(&fullmode, "full", "", false, "Analyze all MRs")
	reviewCmd.Flags().BoolVarP(&interest, "interest", "", false, "Only report MRs that are of interest to the user")
	reviewCmd.Flags().StringVar(&username, "user", "", "Specify a user (does not display Todo list or subscribed labels")
	reviewCmd.Flags().BoolVarP(&palette, "palette", "", false, "Show .config palette colors. (See README.md for instructions)")
}
