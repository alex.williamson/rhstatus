VERSION ?= 0.2.0
GOURL ?= gitlab.com/prarit/rhstatus

build:
	GO111MODULE=on go build -ldflags "-X \"main.version=$(VERSION)\"" $(GOURL)
	@sed -i 's|version=.*|version=$(VERSION)</h1>|' version.html
	@status=$$(git status --porcelain version.html); \
	if test "x$${status}" != x; then \
		echo "Working directory is dirty.  Did you forget to commit the updated version.html file?" >&2; \
	fi
install: build
	GO111MODULE=on go install $(GOURL)

.PHONY: build install
